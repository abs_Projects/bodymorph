/*-----LOGIN FORM VALIDATE ENGINE JS-----*/
$(document).ready( function() {
	$("#userLogin").validationEngine();
});


$('#user_login').on('click',function(){
	$.ajax({
        type: "POST",
        url: HTTP_PATH+"login",
        datatype: "json",
        async: false,
        data: $('#userLogin').serialize(),
        success: function(data) {
            var res = JSON.parse(data);
            if(res.errors == 0){
            	location.href = HTTP_PATH + "Dashboard";
            }else if(res.errors == 1){
            	$('#username').html(res.message.username);
            	$('#password').html(res.message.password);
            }
        }
    });
});
