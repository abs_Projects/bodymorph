<body>
		<div class="wrapper">
			<header>
				<div class="top-header">
					<div class="container inner-wrapper">
						<div class="top-left-header pull-left">
							<ul>
								<li><a href="#"><i class="fa fa-phone"></i> +01 584 2639</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> Contact us</a></li>
								<li>
								<?php 
									$user_id = $this->session->userdata('user_id');
									if( $user_id){
										echo "<a href='Logout'>";
									}else{
										echo "<a href='Registration'>";
									}
									?>								
									
										<i class="fa fa-user"></i> 
										<?php 
											$user_id = $this->session->userdata('user_id');
											if( $user_id){
												echo "Logout";
											}else{
												echo "Register";
											}
											?>
									</a>
								</li>
								<li><a href="#"> English </a></li>
							</ul>
						</div>
						<div class="top-right-header pull-right">
							<div class="social-icon">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
									<li><a href="#"><i class="fa fa-skype"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="container inner-wrapper">
					<div class="bot-header animated fadeIn wow">
						<div class="col-md-3 col-sm-3 logo-box">
							<div class="logo row"><a href="<?php echo HTTP_PATH;?>"><img src="<?php echo FRONT_IMG_PATH;?>logo.png" alt="Todoboda"/></a></div>
						</div>
						<div class="col-md-9 col-sm-9 text-right right-head">
							<div class="row">
								<div class="menuu">
									<div class="">
										<nav class="navbar navbar-default">
											<div class="navbar-header">
												<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
											</div>
											<div class="collapse navbar-collapse js-navbar-collapse">
												<ul class="nav navbar-nav">
													<li class="<?php if($title=="Home")echo "active";?> animated fadeInUp wow"> <a href="<?php echo HTTP_PATH;?>" >Home</a> </li>
													<li class="<?php if($title=="About")echo "active";?> animated fadeInUp wow"><a href="<?php echo HTTP_PATH;?>About" >About Body Morph</a>	</li>
													<li class="<?php if($title=="Gallery")echo "active";?> animated fadeInUp wow"><a href="<?php echo HTTP_PATH;?>Gallery" >Gallery</a></li>
													<li class="<?php if($title=="Blog")echo "active";?> animated fadeInUp wow"><a href="<?php echo HTTP_PATH;?>Blog" >Blog </a></li>
													<li class="<?php if($title=="Contact")echo "active";?> animated fadeInUp wow"><a href="<?php echo HTTP_PATH;?>Contact" >Contact </a></li>
												</ul>
											</div>
											<!-- /.nav-collapse --> 
										</nav>
										<div class="search">
											<a class="show_hide" href="#"><i class="fa fa-search"></i></a>
											<div class="slidingDiv">
												<div class="search-box">
													<input type="search" value="search"/>
													<a href="#" class="show_hide"><i class="fa fa-times"></i></a>
												</div>
												<!--search-box-->
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="clearfix"></div>
										<div></div>
									</div>
									<!--mennu-->
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
						<!--bot-header--> 
					</div>
					<!--container--> 
				</div>
			</header>