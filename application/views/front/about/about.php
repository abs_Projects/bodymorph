	<div class="banner animated fadeIn wow">
		<div class="banner-img">
		<img src="<?php echo FRONT_IMG_PATH;?>banner1.jpg" alt=""/>
		<div class="overlay">
		<div class="container inner-wrapper"><h1>About</h1>
		</div>
		</div>
		</div> <!--banner-img-->
		
		
	</div>
	<!--banner-->
	<div class="breadcrumb-custom">
	<div class="container inner-wrapper">
	<ol class="breadcrumb">
  <li>You are here:</li>
  <li><a href="<?php echo HTTP_PATH;?>">Home</a></li>
  <li class="active">About</li>
</ol>
	</div>
	</div>
<section class="second-section about-second-section animated fadeIn wow">
<div class="container inner-wrapper">
<div class="left-img col-md-5 col-sm-5">
<div class="row">
<img src="<?php echo FRONT_IMG_PATH;?>ab1.jpg" alt=""/>
</div>

</div>

<div class="right-section col-md-7 col-sm-7">
<div class="row">
</div>
<p>The BodyMorph system is a web-delivered program used by industry leading health professionals around the world to assess and deliver 
the most individually tailored health plans to their clients. </p>

<p>Based on data that you enter about yourself, BodyMorph uses the most advanced methods of calculating your physical and nutritional needs. <br/>
You then have the flexibility to modify programs based around what suits your lifestyle and preferences.
Your nutritional plan and exercise regime are tailored to what you want and what you are capable of maintaining. <br/>
No more health kicks and half hearted resolutions that are over before they begin, BodyMorph works because it is simple, flexible and can easily fit into your busy life.</p>

<p>The best part about this program is that you don't need to be a health buff to get started! <br/>
With all the misconceptions out about what's healthy and what's not, BodyMorph stands out from the pack giving you key information in it's simplest forms.<br/>
You then have on-going support from our world class health strategists, via correspondence or in person, who regularly check in to make sure you're on the right track <br/>
and making consistent progress towards your health and fitness goals (whatever they may be).</p>

<p>A healthier, happier life is now just a few clicks away.</p>

</div>

<div class="clearfix"></div>

</div> 

</section>





<section class=" get-started animated fadeIn wow">
<div class="container inner-wrapper">
<div class="heading">
<h1>Get Started Today</h1>

</div> <!--heading ends-->
<div class="get-started-content">
<div class="col-md-3 col-sm-3 get-left">
<img src="<?php echo FRONT_IMG_PATH;?>man.png" alt=""/>

</div>
<div class="col-md-6 col-sm-6 get-center">
<div class="get-form">
<form>
<div class="col-md-6 col-sm-6 form-left">
<div class="form-group">
    <input type="text" placeholder="First Name" class="form-control">
  </div> <!--form-group-->
</div>

<div class="col-md-6 col-sm-6 form-right">
<div class="form-group">
    <input type="text" placeholder="Last Name" class="form-control">
  </div> <!--form-group-->
</div>
<div class="col-md-6 col-sm-6 form-right">
<div class="form-group">
    <input type="text" placeholder="User Name" class="form-control">
  </div> <!--form-group-->
</div>
<div class="col-md-6 col-sm-6 form-left">
<div class="form-group">
    <input type="email" placeholder="Email Address" class="form-control">
  </div> <!--form-group-->
</div>

<div class="col-md-6 col-sm-6 form-right">
<div class="form-group">
    <input type="password" placeholder="Password" class="form-control">
  </div> <!--form-group-->
</div>
<div class="col-md-6 col-sm-6 form-left">
<div class="form-group">
    <input type="password" placeholder="Reenter Password" class="form-control">
  </div> <!--form-group-->
</div>


<div class="clearfix"></div>
<div class="blue-btn"><button type="submit" class="btn">Join Us</button></div>
</form>
</div>

</div>
<div class="col-md-3 col-sm-3 get-right">
<img src="<?php echo FRONT_IMG_PATH;?>woman.png" alt=""/>
</div>
<div class="clearfix"></div>
</div>

</div> 
</section>
<section class="quote animated fadeIn wow">
<div class="container inner-wrapper">
<div class="pull-left quote-left">
<h2>Lorem ipsum sit amet dollor is the dummy text</h2>

<p>We recommend this layout for small or big companies. It is perfect for displaying your business values and attracting your visitors.</p>
</div>
<div class="pull-right quote-right blue-btn">
<button class="btn">Get a quote</button>
</div>
<div class="clearfix"></div>
</div>
</section>
