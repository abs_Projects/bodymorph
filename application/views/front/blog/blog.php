	<div class="banner animated fadeIn wow">
		<div class="banner-img">
		<img src="<?php echo FRONT_IMG_PATH;?>banner1.jpg" alt=""/>
		<div class="overlay">
		<div class="container inner-wrapper"><h1>Blog</h1>
		</div>
		</div>
		</div> <!--banner-img-->
		
		
	</div>
	<!--banner-->
	<div class="breadcrumb-custom">
	<div class="container inner-wrapper">
	<ol class="breadcrumb">
  <li>You are here:</li>
  <li><a href="<?php echo HTTP_PATH;?>">Home</a></li>
  <li class="active">Blog</li>
</ol>
	</div>
	</div>
<section class="second-section blog-section-box animated fadeIn wow">
<div class="container inner-wrapper">

<div class="blog-content" id="blog-page">
				<div class="item">
					<div class=" blog-box">
<div class="blog-image">
<img src="<?php echo FRONT_IMG_PATH;?>b1.jpg" alt=""/>
</div> <!--blog-image-->
<div class="blog-text">
<div class="blog-head">

<h5>Need workout motivation? Try a virtual partner  </h5>

<div class="blog-top">
<div class="blog-left">
<span class="month">June 6</span>
<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
<div class="clearfix"></div>
</div>
<div class="blog-right">
<i class="fa fa-heart-o"></i> 5
</div>
<div class="clearfix"></div>
</div>

</div> <!--blog-head ends-->
<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
<div class="bloger-name">
<h5>Janelle Baptist</h5>
<div class="blog-view-like">
<div class="views">23 view <i class="fa fa-comment"></i></div>
<div class="likes">50 Like <i class="fa fa-thumbs-up"></i></div>
</div>
<div class="clearfix"></div>

</div>

</div> <!--blog-text-->
</div>
				</div>
				<div class="item">
					<div class=" blog-box">
<div class="blog-image">
<img src="<?php echo FRONT_IMG_PATH;?>b4.jpg" alt=""/>
</div> <!--blog-image-->
<div class="blog-text">
<div class="blog-head">

<h5>10 healthiest tips for both men and women </h5>

<div class="blog-top">
<div class="blog-left">
<span class="month">June 6</span>
<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
<div class="clearfix"></div>
</div>
<div class="blog-right">
<i class="fa fa-heart-o"></i> 5
</div>
<div class="clearfix"></div>
</div>

</div> <!--blog-head ends-->
<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
<div class="bloger-name">
<h5>Janelle Baptist</h5>
<div class="blog-view-like">
<div class="views">23 view <i class="fa fa-comment"></i></div>
<div class="likes">50 Like <i class="fa fa-thumbs-up"></i></div>
</div>
<div class="clearfix"></div>

</div>

</div> <!--blog-text-->
</div>
				</div>
				<div class="item">
					<div class=" blog-box">
<div class="blog-image">
<img src="<?php echo FRONT_IMG_PATH;?>b5.jpg" alt=""/>
</div> <!--blog-image-->
<div class="blog-text">
<div class="blog-head">

<h5>The do’s and don’ts of running for fitness </h5>

<div class="blog-top">
<div class="blog-left">
<span class="month">June 6</span>
<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
<div class="clearfix"></div>
</div>
<div class="blog-right">
<i class="fa fa-heart-o"></i> 5
</div>
<div class="clearfix"></div>
</div>

</div> <!--blog-head ends-->
<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
<div class="bloger-name">
<h5>Janelle Baptist</h5>
<div class="blog-view-like">
<div class="views">23 view <i class="fa fa-comment"></i></div>
<div class="likes">50 Like <i class="fa fa-thumbs-up"></i></div>
</div>
<div class="clearfix"></div>

</div>

</div> <!--blog-text-->
</div>
				</div>
				
				
				
				
				<div class="item">
					<div class=" blog-box">
<div class="blog-image">
<img src="<?php echo FRONT_IMG_PATH;?>b1.jpg" alt=""/>
</div> <!--blog-image-->
<div class="blog-text">
<div class="blog-head">

<h5>Need workout motivation? Try a virtual partner  </h5>

<div class="blog-top">
<div class="blog-left">
<span class="month">June 6</span>
<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
<div class="clearfix"></div>
</div>
<div class="blog-right">
<i class="fa fa-heart-o"></i> 5
</div>
<div class="clearfix"></div>
</div>

</div> <!--blog-head ends-->
<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
<div class="bloger-name">
<h5>Janelle Baptist</h5>
<div class="blog-view-like">
<div class="views">23 view <i class="fa fa-comment"></i></div>
<div class="likes">50 Like <i class="fa fa-thumbs-up"></i></div>
</div>
<div class="clearfix"></div>

</div>

</div> <!--blog-text-->
</div>
				</div>
				<div class="item">
					<div class=" blog-box">
<div class="blog-image">
<img src="<?php echo FRONT_IMG_PATH;?>b7.jpg" alt=""/>
</div> <!--blog-image-->
<div class="blog-text">
<div class="blog-head">

<h5>10 healthiest tips for both men and women </h5>

<div class="blog-top">
<div class="blog-left">
<span class="month">June 6</span>
<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
<div class="clearfix"></div>
</div>
<div class="blog-right">
<i class="fa fa-heart-o"></i> 5
</div>
<div class="clearfix"></div>
</div>

</div> <!--blog-head ends-->
<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
<div class="bloger-name">
<h5>Janelle Baptist</h5>
<div class="blog-view-like">
<div class="views">23 view <i class="fa fa-comment"></i></div>
<div class="likes">50 Like <i class="fa fa-thumbs-up"></i></div>
</div>
<div class="clearfix"></div>

</div>

</div> <!--blog-text-->
</div>
				</div>
				<div class="item">
					<div class=" blog-box">
<div class="blog-image">
<img src="<?php echo FRONT_IMG_PATH;?>b6.jpg" alt=""/>
</div> <!--blog-image-->
<div class="blog-text">
<div class="blog-head">

<h5>The do’s and don’ts of running for fitness  </h5>

<div class="blog-top">
<div class="blog-left">
<span class="month">June 6</span>
<span class="comment"><i class="fa fa-comments-o"></i> 5</span>
<div class="clearfix"></div>
</div>
<div class="blog-right">
<i class="fa fa-heart-o"></i> 5
</div>
<div class="clearfix"></div>
</div>

</div> <!--blog-head ends-->
<p>Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor Donec laoreet, enim quis feugiat facilisis, lectus ligula auctor dui, a rutrum dolor....</p>
<div class="bloger-name">
<h5>Janelle Baptist</h5>
<div class="blog-view-like">
<div class="views">23 view <i class="fa fa-comment"></i></div>
<div class="likes">50 Like <i class="fa fa-thumbs-up"></i></div>
</div>
<div class="clearfix"></div>

</div>

</div> <!--blog-text-->
</div>
				</div>
				

</div> <!--#blog-page-->

<div class="blue-btn">
<button type="submit" class="btn"> Load More</button>
</div>


</div> 

</section>






<section class="quote animated fadeIn wow">
<div class="container inner-wrapper">
<div class="pull-left quote-left">
<h2>Lorem ipsum sit amet dollor is the dummy text</h2>

<p>We recommend this layout for small or big companies. It is perfect for displaying your business values and attracting your visitors.</p>
</div>
<div class="pull-right quote-right blue-btn">
<button class="btn">Get a quote</button>
</div>
<div class="clearfix"></div>
</div>
</section>
