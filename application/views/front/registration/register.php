	<div class="breadcrumb-custom">
	<div class="container inner-wrapper">
	<ol class="breadcrumb">
  <li>You are loged in    <span> /</span> </li>
  <li class="active">How to use</li>
</ol>
	</div>
	</div>
<section class="Register login-inner-pages animated fadeIn wow">
<div class="container inner-wrapper">
<div class="register-container">
<div class="register-head">
Register yourself
</div> <!--register-head-->
<div class="register-box">

<div class="col-md-12">
  <?php echo ($this->session->flashdata("success")) ? "<h5>".$this->session->flashdata("success")."</h5>" : "" ; ?>
</div>
<form id="user_registartion" action="javascript:void(0)" method="post">
<!-- <form id="user_registartion" action="javascript:void(0);" method="post"> -->

    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label>User Name *</label>
        <input type="text" placeholder="Enter User Name..." name="user_email" value="<?php echo set_value('user_email'); ?>" class="form-control validate[required]">
        <?php echo form_error('user_email','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> First Name *</label>
        <input type="text" placeholder="Enter First Name..." name="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control validate[required]">
        <?php echo form_error('first_name','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> Surname *</label>
        <input type="text" placeholder="Enter Surname..." name="sur_name" value="<?php echo set_value('sur_name'); ?>" class="form-control validate[required]">
        <?php echo form_error('sur_name','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> Password *</label>
        <input type="password" placeholder="Enter Prefered Password" name="password" value="<?php echo set_value('password'); ?>" class="form-control validate[required]">
        <?php echo form_error('password','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label>Mobile *</label>
        <input type="text" placeholder="Enter Your Mobile Number" name="mobile" value="<?php echo set_value('mobile'); ?>" class="form-control validate[required]">
        <?php echo form_error('mobile','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
    <div class="col-md-4 col-sm-4 regiter-input">
    <div class="form-group">
       <label>Gender * </label>
          <select class="validate[required] form-control" name="gender">
            <option value="">Select Gender</option>
            <option value="M">Male</option>
            <option value="F">Female</option>
        </select>
        <?php echo form_error('gender','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->
    </div> <!--regiter-input ends-->

    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> Country *</label>
          <select class="validate[required] form-control" name="country">
              <option value="">Select Country</option>
                <?php foreach ($countries as $country) { ?>
              <option value="<?php echo $country['country_id'];?>"><?php echo $country['country_name'];?></option>
               <?php } ?>
          </select>
        <?php echo form_error('country','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->
    </div> <!--regiter-input ends-->

    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> postcode *</label>
        <input type="text" placeholder="Enter postal code" name="postcode" value="<?php echo set_value('postcode'); ?>" class="form-control validate[required]">
        <?php echo form_error('postcode','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
<!--     <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> First Date *</label>
       <div class='input-group date' id=''>
                        <input type='text' class="form-control" value="<?php echo set_value('first_date'); ?>" name="first_date" id="first_date"/>
                        <span class="input-group-addon">
                            <span class="glyphicon"><i class="fa fa-calendar"></i></span>
                        </span>
                    </div>
                    <?php //echo form_error('first_date','<div class="alert-danger">','</div>'); ?>
    </div>
     

    </div>-->
    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label> Date Of Birth *</label>
        <div class='input-group date' id=''>
                        <input type='text' class="form-control validate[required]" value="<?php echo set_value('date_of_birth'); ?>" name="date_of_birth" id="date_of_birth"/>
                        <span class="input-group-addon">
                            <span class="glyphicon"><i class="fa fa-calendar"></i></span>
                        </span>
                    </div>
                     <?php echo form_error('date_of_birth','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->
    <div class="col-md-4 col-sm-4 regiter-input">
       <div class="form-group">
       <label>How Did You Here About Us </label>
        <select class="validate[required]" name="how_did_you_know">
          <option value="1">Friend</option>
          <option value="2">Social media</option>
          <option value="3">Radio</option>
        </select>
        <?php echo form_error('how_did_you_know','<div class="alert-danger">','</div>'); ?>
    </div>
     <!--input-group-->

    </div> <!--regiter-input ends-->

    <div class="clearfix"></div>

    <div class="blue-btn"> <button class="btn" type="submit" id="submit_registartion" >Sumit</button></div>
</form>

</div> <!--register-box ends-->
<div class="register-footer">
</div> <!--register-footer-->



</div>

</div>
</section>

