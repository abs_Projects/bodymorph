<div class="banner animated fadeIn wow">
		<div class="banner-img">
		<img src="<?php echo FRONT_IMG_PATH;?>banner1.jpg" alt=""/>
		<div class="overlay">
		<div class="container inner-wrapper">
		</div>
		</div>
		</div> <!--banner-img-->
		
		
	</div>
	<!--banner-->
	<div class="breadcrumb-custom">
	<div class="container inner-wrapper">
	<ol class="breadcrumb">
  <li>You are here:</li>
  <li><a href="<?php echo HTTP_PATH;?>">Home</a></li>
  <li class="active">Gallery</li>
</ol>
	</div>
	</div>
<section class="gallery-section animated fadeIn wow">
<div class="container inner-wrapper">

<div class="filter">
<ul>
<li class="active"><a href="#">All <span>12</span></a></li>
<li><a href="#">Nutritions <span>4</span></a></li>
<li><a href="#">Training <span>3</span></a></li>
<li><a href="#">achievers <span>5</span></a></li>
</ul>
</div> <!--filter-->

<div class="gallery-box">
<div class="gallery-image">
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g1.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g2.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g3.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g4.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g5.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g6.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g7.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g8.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g9.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>plus.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="clearfix"></div>
</div>

<div class="gallery-video">
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g4.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>arrow.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g5.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>arrow.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>
<div class="col-md-4 col-sm-4 gallery-1">
<div class="row">
<div class="galary-image">
<img src="<?php echo FRONT_IMG_PATH;?>g6.jpg" alt=""/>
<div class="overlay-gal">
<img src="<?php echo FRONT_IMG_PATH;?>arrow.png" alt=""/>
</div> <!--overlay-gal ends-->

</div>
</div>
</div>

<div class="clearfix"></div>
</div>

<div class="pagination-custom">
<nav>
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a class="active" href="#">1</a></li>
    <li><a  href="#">2</a></li>
    <li><a href="#">3</a></li>
    
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>

</div>

</div> <!--gallery-box ends-->




<div class="clearfix"></div>

</div> 

</section>






<section class="quote animated fadeIn wow">
<div class="container inner-wrapper">
<div class="pull-left quote-left">
<h2>Lorem ipsum sit amet dollor is the dummy text</h2>

<p>We recommend this layout for small or big companies. It is perfect for displaying your business values and attracting your visitors.</p>
</div>
<div class="pull-right quote-right blue-btn">
<button class="btn">Get a quote</button>
</div>
<div class="clearfix"></div>
</div>
</section>
